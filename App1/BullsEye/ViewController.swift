//
//  ViewController.swift
//  BullsEye
//
//  Created by Muhammad Shahzain on 02/08/2019.
//  Copyright © 2019 Muhammad Shahzain. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    var currentValue: Int = 50
    var targetValue: Int = 0
    var round: Int = 0
    var score: Int = 0
    @IBOutlet weak var slider : UISlider!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    
    @IBOutlet weak var roundLabel: UILabel!
    override func viewDidLoad()
    {
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
        super.viewDidLoad()
        startNewRound()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func startNewRound()
    {
        round += 1
        targetValue = 1 + Int(arc4random_uniform(100))
        
        currentValue = 50
        slider.value = Float(currentValue)
        updateLabels()
        
    }
    func updateLabels()
    {
        targetLabel.text = String(targetValue)
        roundLabel.text = String(round)
        scoreLabel.text = String(score)
        
    }
    @IBAction func sayAwesomeAlert()
    {
        let difference = abs(targetValue - currentValue)
        let points = 100 - difference
        score += points
        let title: String
        if difference == 0 {
            title = "Perfect!"
        }
        else if difference < 5
        {
            title = "Pretty awesome"
        }
        else if difference < 10
        {
            title = "Uh you were fair"
        }
        else
        {
            title = "You weren't even close"
        }
        let message = "You scored \(points) points"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Okay", style: .default, handler: {action in self.startNewRound()})
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        

    }
    @IBAction func sliderMoved(_ slider: UISlider)
    {
        currentValue = lroundf(slider.value)

    }
    

}


