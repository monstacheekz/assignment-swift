import Foundation
//Task1

func repeatTask(times:  Int, task: () -> Void)
{
    for i in 1...times
    {
        task()
    }

}
repeatTask(times: 10, task: {print("S-wift Apprentice is a nice book! ")})

//Task2

func MathSum(length: Int, series: (Int) -> Int) -> Int
{
    var result = 0
    for i in 1...length
    {
        result += series(i)
    }
    return result
}
MathSum(length: 10){j in
 j * j}

 func fib(_ number: Int) -> Int{
     if number <= 0{
         return 0
     }
     if number == 1 || number == 2{
         return 1
     }

     return fib (number - 1) + fib(number - 2)
 }
print(MathSum(length: 10, series: fib))


let AppRatings = [
"Calendar Pro": [1, 5, 5, 4, 2, 1, 5, 4],
"The Messenger": [5, 4, 2, 5, 4, 1, 1, 2],
"Socialise": [2, 1, 2, 2, 1, 2, 4, 2]
]

